#!/usr/bin/python

from copy import copy
from optparse import OptionParser, OptionValueError
import pprint
from random import seed, randint
import struct
from socket import *
from sys import exit, maxint as MAXINT
from time import time, sleep

from gz01.collections_backport import OrderedDict
from gz01.dnslib.RR import *
from gz01.dnslib.Header import Header
from gz01.dnslib.QE import QE
from gz01.inetlib.types import *
from gz01.util import *

# timeout in seconds to wait for reply
TIMEOUT = 5

# domain name and internet address of a root name server
ROOTNS_DN = "f.root-servers.net."   
ROOTNS_IN_ADDR = "192.5.5.241"

class ACacheEntry:
  ALPHA = 0.8

  def __init__(self, dict, srtt = None):
    self._srtt = srtt
    self._dict = dict

  def __repr__(self):
    return "<ACE %s, srtt=%s>" % \
      (self._dict, ("*" if self._srtt is None else self._srtt),)

  def update_rtt(self, rtt):
    old_srtt = self._srtt
    self._srtt = rtt if self._srtt is None else \
      (rtt*(1.0 - self.ALPHA) + self._srtt*self.ALPHA)
    logger.debug("update_rtt: rtt %f updates srtt %s --> %s" % \
       (rtt, ("*" if old_srtt is None else old_srtt), self._srtt,))

class CacheEntry:
  def __init__(self, expiration = MAXINT, authoritative = False):
    self._expiration = expiration
    self._authoritative = authoritative

  def __repr__(self):
    now = int(time())
    return "<CE exp=%ds auth=%s>" % \
           (self._expiration - now, self._authoritative,)

class CnameCacheEntry:
  def __init__(self, cname, expiration = MAXINT, authoritative = False):
    self._cname = cname
    self._expiration = expiration
    self._authoritative = authoritative

  def __repr__(self):
    now = int(time())
    return "<CCE cname=%s exp=%ds auth=%s>" % \
           (self._cname, self._expiration - now, self._authoritative,)




# >>> entry point of ncsdns.py <<<

# Seed random number generator with current time of day:
now = int(time())
seed(now)

# Initialize the pretty printer:
pp = pprint.PrettyPrinter(indent=3)

# Initialize the name server cache data structure; 
# [domain name --> [nsdn --> CacheEntry]]:
nscache = dict([(DomainName("."), 
            OrderedDict([(DomainName(ROOTNS_DN), 
                   CacheEntry(expiration=MAXINT, authoritative=True))]))])

# Initialize the address cache data structure;
# [domain name --> [in_addr --> CacheEntry]]:
acache = dict([(DomainName(ROOTNS_DN),
           ACacheEntry(dict([(InetAddr(ROOTNS_IN_ADDR),
                       CacheEntry(expiration=MAXINT,
                       authoritative=True))])))]) 

# Initialize the cname cache data structure;
# [domain name --> CnameCacheEntry]
cnamecache = dict([])

# Parse the command line and assign us an ephemeral port to listen on:
def check_port(option, opt_str, value, parser):
  if value < 32768 or value > 61000:
    raise OptionValueError("need 32768 <= port <= 61000")
  parser.values.port = value

parser = OptionParser()
parser.add_option("-p", "--port", dest="port", type="int", action="callback",
                  callback=check_port, metavar="PORTNO", default=0,
                  help="UDP port to listen on (default: use an unused ephemeral port)")
(options, args) = parser.parse_args()

# Create a server socket to accept incoming connections from DNS
# client resolvers (stub resolvers):
ss = socket(AF_INET, SOCK_DGRAM)
ss.bind(("127.0.0.1", options.port))
serveripaddr, serverport = ss.getsockname()

# NOTE: In order to pass the test suite, the following must be the
# first line that your dns server prints and flushes within one
# second, to sys.stdout:
print "%s: listening on port %d" % (sys.argv[0], serverport)
sys.stdout.flush()

# Create a client socket on which to send requests to other DNS
# servers:
setdefaulttimeout(TIMEOUT)
cs = socket(AF_INET, SOCK_DGRAM)

# Clean up cache
def cache_cleanup():
  return

# Lookup in cache
def lookup_cache(nameserver):
  return

# Add nameserver into cache
def add_cache(nameserver):
  return

# Get all NS
def get_ns(query, count=0):
  ns = []
  next_count = 0
  question = QE.fromData(query, 12)
  for x in range(0, count):
    authority = RR_NS.fromData(query, len(question) + 12 + next_count)
    #print str(authority)
    ns.append(authority[0])
    next_count = next_count + authority[1]
  return (ns, next_count)

# Get all additional information
def get_additional(query, count=0, offset=0):
  additional = []
  next_count = 0
  question = QE.fromData(query, 12)
  for x in range(0, count):
    add = RR_A.fromData(query, len(question) + 12 + offset + next_count)
    additional.append(add[0])
    print add
    next_count = next_count + add[1]
  return (additional, next_count)


# Construct DNS message to send dns query to server
def dns_construct(query):
  dns = ""
  header = Header.fromData(query)
  print str(header)
  question = QE().fromData(query, 12)
  print str(question)
  return

# This is a simple, single-threaded server that takes successive
# connections with each iteration of the following loop:
while 1:
  (data, address,) = ss.recvfrom(512) # DNS limits UDP msgs to 512 bytes
  if not data:
    log.error("client provided no data")
    continue
  
  #
  # TODO: Insert code here to perform the recursive DNS lookup;
  #       putting the result in reply.
  #

  # Show the next route
  next_route_ip = ROOTNS_IN_ADDR
  next_route_dn = ROOTNS_DN

  print "Received:"
  print hexdump(data)

  print str(Header.fromData(data))

  ns_list = None
  add_list = None

  while True:
    count_authority = 0
    count_addtional = 0

    if next_route_ip is ROOTNS_IN_ADDR and next_route_dn is ROOTNS_DN:
      #query = dns_construct(data)
      cs.sendto(data,(ROOTNS_IN_ADDR,53))
      (data, addr) = cs.recvfrom(512)
    else:
      try:
        cs.sendto(data,(next_route_ip, 53))
        (data, addr) = cs.recvfrom(512)
      except:
        raise
    print "Received send:"
    print hexdump(data)
    # Inspect packet for answer
    header = Header.fromData(data)
    answer_count = header._ancount
    print "Answer count:", answer_count
    if answer_count == 1:
      # Answer exists break
      print str(data)
      break
    elif answer_count == 0:
      print "No answer continue"
      (ns_list, ns_offset) = get_ns(data, count=header._nscount)
      (add_list, glue_offset) = get_additional(data, count=header._arcount, offset=ns_offset)
      # Access Authority section (NS)
      print "NS"
      print ns_list
      print "Additional"
      print add_list
      # Look into caches
      # Access Glue section if possible
    else:
      print "Unexpected possibility!"
      pass
  
  # Process:
  # 1. DNS will lookup domain edu in the cache first. If it does not exist it will query.
  # 2. DNS will query one of the edu authorities for scholarly.
  # 3. Send query answer to Client
  #
  #print "Client address:\n", address
  #queryheader = Header.fromData(data, 0)
  
  #question_sec = QE.fromData(data, 0)
  #print "Question:", hexdump(question_sec.pack())
  #print "Unpacked query header received from client is:\n", queryheader
  #print "Query header received from client is:\n", hexdump(queryheader.pack())
  #print "Query header received from client in string::\n", str(queryheader[0])

  # Split query into domain points
  
  # Go from tail to head of the domain tokens and resolve each one of them
  
  reply = data
  
  
  logger.log(DEBUG2, "our reply in full:") 
  logger.log(DEBUG2, hexdump(reply))

  ss.sendto(reply, address)
cs.close()
ss.close()